import math
import random
import numpy as np
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from palettable.colorbrewer.qualitative import Dark2_8
from palettable.cartocolors.diverging import Earth_3

################################################################################
############ Definition of the tree, node and trend classes + utils ############
################################################################################

# Simple node class
class Node(object):
    def __init__(self, id, startTime=0.):
        self.id = id
        self.bl = round(1.e-5+random.random(), 3)
        self.startTime = startTime
        self.endTime = round(self.startTime+self.bl, 3)
        self.iTrend = 0 # identifier for the tend
        self.parent = None
        self.children = []

    def add_child(self, obj):
        self.children.append(obj)
        obj.parent = self

    # Recursively set trends to descendant nodes
    def recSetTrend(self, newTrend):
        self.intRecSetTrend(newTrend, self.iTrend)

    def intRecSetTrend(self, newTrend, origTrend):
        for c in self.children:
            c.intRecSetTrend(newTrend, origTrend)

        if self.iTrend == origTrend:
            self.iTrend = newTrend

    def toStr(self):
        aStr = repr(self.id) + " : "
        aStr += repr(self.bl)
        aStr += " ( " + repr(self.startTime) + " => " + repr(self.endTime) + " ) : "
        aStr += repr(self.iTrend) + " : "
        aStr += "[ " + ', '.join([repr(c.id) for c in self.children]) + " ]"
        return aStr

    # Compute the trajectory for the expected BM average along the branch
    def computeMuTrajectory(self, trends):
        timeRange = np.linspace(self.startTime, self.endTime, 100)
        aTrend = trends.getTrend(self.iTrend)
        muVal = aTrend.muT(timeRange)
        return [timeRange, muVal]


# Define a trend (expected BM average trajectory)
class Trend(object):
    def __init__(self, coeffs, timeOffset=0.):
        # Contains 3 coefficients. c[0] + c[1] * t + c[2] * t**2
        self.coeffs = coeffs
        # Time offset, that is when this trend begins (offset >= 0)
        self.timeOffset = timeOffset

    # Compute the expected BM average for a given time t
    def muT(self, t):
        return self.coeffs[0] + self.coeffs[1]*(t-self.timeOffset) + self.coeffs[2]*((t-self.timeOffset)**2)

# Trends container
class Trends(object):
    def __init__(self):
        self.container = {}
        self.container[0] = Trend([0.,0.,0.])
        self.cntTrends = 1

    def addTrend(self, aTrend):
        iTrend = self.cntTrends
        self.container[iTrend] = aTrend
        self.cntTrends += 1
        return iTrend

    def getTrend(self, id):
        return self.container[id]

    def updateTrend(self, id, aTrend):
        self.container[id] = aTrend

    def remTrend(self, id):
        del self.container[id]

# Tree class
class Tree(object):
    def __init__(self, nTips):
        self.nTips = nTips
        self.nBranches = 2*nTips-2
        self.root = Node(0)
        self.nodes = [self.root]
        self.terminalNodes = [self.root]
        self.trends = Trends()

        self.createTree()

    # Tree constructor
    def createTree(self):
        def create_child(aNode):
            newNode = Node(len(self.nodes), aNode.endTime)
            self.nodes.append(newNode)
            self.terminalNodes.append(newNode)
            aNode.add_child(newNode)

        while len(self.terminalNodes) < self.nTips:
            curNode = self.terminalNodes.pop(0)
            create_child(curNode) # left
            create_child(curNode) # right

    # Add a trend to a given node and its descendant (that are under the same initial trend)
    def addTrend(self, iNode, trend):
        iTrend = self.trends.addTrend(trend)
        identifyNodeById = lambda aNode : iNode == aNode.id
        filteredNodes = filter(identifyNodeById, self.nodes)
        for aNode in filteredNodes: aNode.recSetTrend(iTrend)
        return iTrend

    def updateTrend(self, iTrend, trend):
        self.trends.updateTrend(iTrend, trend)

    def remTrend(self, iTrendRem):
        if iTrendRem == 0: return

        self.trends.remTrend(iTrendRem)

        oldestNode = list(filter(lambda aNode: aNode.iTrend == iTrendRem and aNode.parent.iTrend != iTrendRem, self.nodes))[0]
        affectedNodes = list(filter(lambda aNode: aNode.iTrend == iTrendRem, self.nodes))

        for node in affectedNodes:
            node.iTrend = oldestNode.parent.iTrend

    def print(self):
        for aNode in self.nodes:
            print(aNode.toStr())
        print("---------------------------------------------")

    def getNode(self, iNode):
        return list(filter(lambda aNode: aNode.id == iNode, self.nodes))[0]

# Discretize the value of the expected BM average along branches, i.e., trajectories.
def computeMuTrajectories(tree):

    trajectories = {}

    for node in tree.nodes:
        trajectories[node.id] = node.computeMuTrajectory(tree.trends)

    return trajectories

# Get the average "expected BM average" for tips by trends or overall
def getAverageMuTipsPerTrend(tree, trajectories):

    averages = {}
    for keyTrend in tree.trends.container:
        identifyNodeByTrend = lambda aNode : (keyTrend == aNode.iTrend and len(aNode.children) == 0)
        filteredNodes = list(filter(identifyNodeByTrend, tree.nodes))

        if len(filteredNodes) == 0: break

        sum = 0.
        for aNode in filteredNodes: sum += trajectories[aNode.id][1][-1]
        averages[keyTrend] = sum/len(filteredNodes)

    sum=0.
    for node in tree.terminalNodes:
        sum += trajectories[node.id][1][-1]
        #print("test : ", trajectories[node.id][1][-1])
    averages[-1] = sum / len(tree.terminalNodes)
    #print(averages[-1])

    return averages

# Define y positions for the tree plotting utils
def recPosY(node, level, pos, positionsX, positionsY, span=20):

    positionsY[node.id] = pos

    if len(node.children) == 0: return

    positionsX.append([node, node.endTime, pos-span/(2**level), pos+span/(2**level)])
    for c, offset in zip(node.children, [-1.,1.]):

        newPos = pos + offset*span/(2**level)
        recPosY(c, level+1, newPos, positionsX, positionsY, span)

# Plotting trajectories along the tree and so on
def plotTrajectories(tree, trajectories):

    myCmap1 = Dark2_8
    myCmap2 = Earth_3.mpl_colormap

    X_WIDTH = 1200
    Y_WIDTH = 1200
    DPI = 120

    fig, ax = plt.subplots(4, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), dpi=DPI, constrained_layout=False, sharex=False, sharey=False, gridspec_kw={'height_ratios': [3, 1, 1, 1]})

    minMu = 1.e10
    maxMu = -1.e10
    for key in trajectories:
        minMu = np.min([minMu, np.min(trajectories[key][1])])
        maxMu = np.max([maxMu, np.max(trajectories[key][1])])

    norm = matplotlib.colors.Normalize(vmin=minMu, vmax=maxMu)

    for key in trajectories:
        node = tree.getNode(key)
        ax[2].plot(trajectories[key][0], trajectories[key][1], color=myCmap1.hex_colors[1+node.iTrend])

    positionsX = []
    positionsY = {}
    mySpan=50
    recPosY(tree.root, 0, 0., positionsX, positionsY, span=mySpan)

    # Tree with node id
    for node in tree.nodes:
        ax[0].plot([node.startTime, node.endTime], [positionsY[node.id], positionsY[node.id]], color=myCmap1.hex_colors[1+node.iTrend], linewidth=2, zorder=-1)
        ax[0].scatter(node.endTime, positionsY[node.id], s=mySpan*5, color='w', edgecolors=myCmap1.hex_colors[1+node.iTrend], zorder=0)
        ax[0].text(node.endTime, positionsY[node.id], repr(node.id), color='k', zorder=1, ha='center', va='center')

    for [node, xPos, ymin, ymax] in positionsX:
        ax[0].plot([xPos, xPos], [ymin, ymax], color=myCmap1.hex_colors[1+node.iTrend], linewidth=2, zorder=-1)

    # Tree with means
    for node in tree.nodes:
        vals = trajectories[node.id][1]
        colorFlt = norm(vals[int(len(vals)/2)])
        ax[1].plot([node.startTime, node.endTime], [positionsY[node.id], positionsY[node.id]], color=myCmap2(colorFlt), linewidth=2, zorder=-1)

    for [node, xPos, ymin, ymax] in positionsX:
        vals = trajectories[node.id][1]
        colorFlt = norm(vals[-1])
        ax[1].plot([xPos, xPos], [ymin, ymax], color=myCmap2(colorFlt), linewidth=2, zorder=-1)

    for node in tree.nodes:
        vals = trajectories[node.id][1]
        colorFlt = norm(vals[0])
        ax[1].scatter(node.startTime, positionsY[node.id], color=myCmap2(colorFlt), s=20*mySpan/tree.nTips, edgecolors=myCmap1.hex_colors[1+node.iTrend], alpha=1.0, zorder=1)

        #colorFlt = norm(vals[int(len(vals)/2)])
        #ax[1].scatter(node.startTime+(node.endTime-node.startTime)/2., positionsY[node.id], color=myCmap2(colorFlt), s=20*mySpan/tree.nTips, edgecolors='k', alpha=1.0, zorder=1)

        colorFlt = norm(vals[-1])
        ax[1].scatter(node.endTime, positionsY[node.id], color=myCmap2(colorFlt), s=20*mySpan/tree.nTips, edgecolors=myCmap1.hex_colors[1+node.iTrend], alpha=1.0, zorder=1)

    muPerTrend = {'all': []}
    for node in tree.terminalNodes:
        if not node.iTrend in muPerTrend:
            muPerTrend[node.iTrend] = []

        muPerTrend[node.iTrend].append(trajectories[node.id][1][-1])
        muPerTrend['all'].append(trajectories[node.id][1][-1])

    labels = []
    vals = []
    for key, val in muPerTrend.items():
        labels.append(key)
        vals.append(val)

    ax[3].boxplot(vals, vert=True, patch_artist=True, labels=labels)

    plt.colorbar(cm.ScalarMappable(norm=norm, cmap=myCmap2), ax=ax[3])


################################################################################
############                  RJMCMC proposals                      ############
################################################################################

# Define new rates using the Arithmetic mean (PyRate RJMCMC-like)
def proposalRates():
    # Initial rate
    r0=1
    # Random numer - the distribution can be the one of your choice
    u=random.random()
    u2=(1-u)/u
    # New rates
    rij = [math.exp(math.log(r0)+0.5*math.log(u2)), math.exp(math.log(r0)-0.5*math.log(u2))]
    #print(rij)
    # Jacobian
    jacobians1 = [np.sum(rij)*np.sum(rij)/r0, abs(r0/(u*(u-1)))]
    print([r0, u, u2])
    print(jacobians1)

    ## reverse move
    r1 = rij[0]
    r2 = rij[1]

    # Finding back u and r
    u2 = math.exp(math.log(r1)-math.log(r2))
    u = 1./(u2+1)
    r0 = math.exp(math.log(r1)-0.5*math.log(u2))
    print([r0, u, u2])

    # inverse jacobian
    jacobians2 = [1./(np.sum(rij)*np.sum(rij)/r0), 1./(abs(r0/(u*(u-1))))]
    print(jacobians2)

    # check
    for j1, j2 in zip(jacobians1, jacobians2):
        print(j1*j2)


# Define a tree and some trends
def exampleTreeAndTrends():
    ## Tree
    nTips = 25
    tree = Tree(nTips)

    iTrend = 1

    node = tree.getNode(3)
    offsetMu = tree.trends.container[node.iTrend].muT(node.startTime)
    tree.addTrend(node.id, iTrend, Trend([offsetMu, 2., 1.], node.startTime))
    iTrend += 1

    node = tree.getNode(5)
    offsetMu = tree.trends.container[node.iTrend].muT(node.startTime)
    tree.addTrend(node.id, iTrend, Trend([offsetMu, -2, -5], node.startTime))
    iTrend += 1

    node = tree.getNode(7)
    offsetMu = tree.trends.container[node.iTrend].muT(node.startTime)
    tree.addTrend(node.id, iTrend, Trend([offsetMu, 20., 20.], node.startTime))
    iTrend += 1

    trajectories = computeMuTrajectories(tree)

    print(getAverageMuTipsPerTrend(tree, trajectories))

    plotTrajectories(tree, trajectories)

    iTrend = iTrend-1
    tree.remTrend(iTrend)

    trajectories = computeMuTrajectories(tree)
    plotTrajectories(tree, trajectories)


### Below: Slope proposal for trends
# Most of the code below is used to build RJMCMC propsals for the slope:
# > i.e., coefficient "b" of: c + b * t + a * t**2
# > We assume here it that a=0 and c is defined by the ancestor end's trajectory

### The slope proposal aims to maintain the average tips "expected BM average" (condition 1)
# Assuming an initial trend with slope b, we propose two new slopes b' and b''
# The value of these slopes is then defined by "condition 1" and a random value "u"
#
# Condition 1 defines:
# Simplified EQ1: Avg of tips trajectory end point with "b" = avg of tips trajectory end point with "b'" + avg of tips trajectory end point with "b''"
# Full EQ1: [C + f(b)]/nNodes = [C + f(b') + f(b'')]/nNodes
#
# With:
# > b' = (1+alpha) * b
# > b'' = (1+beta) * b
# We can define:
# > alpha = u
# > b' = (1+u) * b
# > u ~ Normal(...)
#
# Then we need to find "beta" using (full) EQ1
# > beta = -f(b') * u / f(b'') = -u * C' / C''
# Now we have all we need for the jacobian:
# > [b, u] => [b', b'']
# > u ~ Normal (or other)
# > b = b
# > b' = (1+b)*u
# > b'' =  -u * C' / C''
#
# The jacobian is Then
# > J = abs[ (C' / C'') * (1+u) ]


# Retun the coefficients for equation EQ1
# We build the list of all root -> terminal edges traversed with all the bl and slopes
def recursiveCoeffs(node, coeffs):

    children = []

    if len(node.children) > 0:
        for child in node.children:
            children += recursiveCoeffs(child, coeffs)
    else:
        children.append(node.id)
        coeffs[node.id] = []

    for child in children:
        coeffs[child].append([node.id, node.bl, node.iTrend])

    return children


# Compute the projected expected BM average at tips given the coefficients
#
def computeProjectedMus(tree, coeffs, children):

    muPerSpecies = {}

    trends = tree.trends
    for c in children:
        rCoeffs = coeffs[c][:]
        rCoeffs.reverse() # From root to tips
        # Init at root value offset (i.e., c from c + b * t + c * t**2)
        result = trends.getTrend(rCoeffs[0][2]).coeffs[0]
        # For each traversed edge
        for rc in rCoeffs:
            # add branch_length times slope
            result += trends.getTrend(rc[2]).coeffs[1]*rc[1]
        muPerSpecies[c] = result

    return muPerSpecies

def reverseMove(slopeB1, slopeB2, coeffB1, coeffB2):

    ## Reverse move:
    # Merge two "trends" with slope B1 and slope B2
    # * CoeffB1 is the sum of branches lenghts under trend 1 (i.e., slope1)
    # * CoeffB2 is the sum of branches lenghts under trend 2 (i.e., slope2)

    # This move is deterministic (b', b'') => (b, u)
    # Define alpha/u
    alpha = (slopeB1-slopeB2)/(slopeB2+(coeffB1/coeffB2)*slopeB1)
    # Define b
    originalSlope = slopeB1/(1.+alpha)
    # Final values
    u = alpha
    m = originalSlope

    # THe jacobian is the inverse of the "forward" jacobian
    term1_1 = 1.+alpha
    term1_2 = m
    term2_1 = 1-alpha*(coeffB1/coeffB2) #-coeffB1/coeffB2 # both slopes in numerator and denumerator simplifies
    term2_2 = -m*(coeffB1/coeffB2) #0.
    jacobian = 1./abs(term1_1*term2_2-term1_2*term2_1)

    print(" u = alpha = ", u)
    print(" original slope = m = ", m)
    print([term1_1, term1_2, term2_1, term2_2])
    print("Jacobian = ", jacobian, " ==> ", 1./jacobian)

# Define the value of Beta
def computeBetaAndJacobian(alpha, origSlope, iTrendOld, iTrendNew, tree, coeffs, children):

    terms = {iTrendOld : 0., iTrendNew : 0., -1 : 0.}

    averageMuOrig = 0
    trends = tree.trends
    for c in children:
        rCoeffs = coeffs[c][:]
        rCoeffs.reverse() # Branches from root to tip node with [iNode, bl / t, iTrend]
        result = trends.getTrend(rCoeffs[0][2]).coeffs[0] # This is the offset, i.e. c in c + b * t + c * t**2
        terms[-1] += result
        for rc in rCoeffs:
            iTrend = rc[2]
            slope = trends.getTrend(iTrend).coeffs[1]
            branchLength = rc[1]

            if iTrend == iTrendNew:
                terms[iTrendNew] += branchLength
            elif iTrend == iTrendOld:
                terms[iTrendOld] += branchLength
            else:
                terms[-1] += slope*branchLength

            result += slope*branchLength

        averageMuOrig += result
    averageMuOrig /= len(children)

    # Beta from simplification
    beta = -alpha*(terms[iTrendOld])/(terms[iTrendNew])

    # Beta from full equation
    #beta2 = (len(children)*averageMuOrig - terms[-1] - (1+alpha)*terms[iTrendOld]*origSlope) / (terms[iTrendNew]*origSlope) - 1.0
    #print("b1 vs b2 : ", beta, " - ", beta2)

    # Jacobian
    m = origSlope
    term1_1 = 1.+alpha
    term1_2 = m
    term2_1 =  1.-alpha*(terms[iTrendOld]/terms[iTrendNew])
    term2_2 = -m*(terms[iTrendOld]/terms[iTrendNew])
    jacobian = abs(term1_1*term2_2-term1_2*term2_1)
    print([term1_1, term1_2, term2_1, term2_2])
    #print("Tentative jacobian : ", jacobian)


    print("*************************************************************")
    print("Original slope = ", origSlope)
    print("Alpha=u : ", alpha)
    print("New slope1 = ", (1.+alpha)*origSlope)
    print("Beta : ", beta)
    print("New slope2 = ", (1.+beta)*origSlope)
    print("Jacobian : ", jacobian)
    print("=============================================================")
    reverseMove((1+alpha)*origSlope, (1+beta)*origSlope, terms[iTrendOld], terms[iTrendNew])
    print("*************************************************************")

    return [beta, jacobian]

# RJMCMC proposal for the slope
def proposeNewSlope(tree):
    mean = 0.
    std = 0.1
    u = 0 #np.random.normal(mean, std, 1)
    alpha = u

    # Drawing a node
    nNodes = len(tree.nodes)
    iNode = np.random.randint(1, nNodes-1, 1)

    # Getting node/Trend info
    curNode = tree.getNode(iNode)
    iCurTrend = curNode.iTrend
    curTrend = tree.trends.getTrend(iCurTrend)
    origSlope = curTrend.coeffs[1]
    #print("Random number : " + repr(u) + " -- random node : " + repr(iNode))

    # Parent node
    parNode = curNode.parent
    otherNode = parNode.children[1] if parNode.children[0] == curNode else parNode.children[0]

    # New trend - temporarily same as before
    offsetMu = curTrend.muT(curNode.startTime)
    newTrend = Trend([offsetMu, origSlope, 0.], curNode.startTime)
    iNewTrend = tree.addTrend(curNode.id, newTrend)

    # Recover the "coefficients", that is the sum of branch lenghts under different trends
    coeffs = {}
    children = recursiveCoeffs(tree.root, coeffs)

    #pMus = computeProjectedMus(tree, coeffs, children)

    # Get the beta and jacobian
    [beta, jacobian] = computeBetaAndJacobian(alpha, origSlope, iCurTrend, iNewTrend, tree, coeffs, children)

    # Update old trend
    newSlope1 = (1.+alpha)*origSlope
    curTrend.coeffs[1] = newSlope1
    tree.updateTrend(iCurTrend, curTrend)

    # New trend: phase 2:
    newTrend.coeffs[0] = curTrend.muT(curNode.startTime) # New offset
    newSlope2 = (1.+beta)*origSlope # new slope
    newTrend.coeffs[1] = newSlope2
    tree.updateTrend(iNewTrend, newTrend)
    pMus = computeProjectedMus(tree, coeffs, children)
    #print("Average mu :", pMus)

    trajectories = computeMuTrajectories(tree)
    plotTrajectories(tree, trajectories)
    print("Average mu traj:", getAverageMuTipsPerTrend(tree, trajectories))

    return [iNode, origSlope, alpha, beta, jacobian]


# Example trend proposal
def trendsProposals():
    ## Tree
    nTips = 25
    tree = Tree(nTips)

    iTrend = 0

    tree.updateTrend(iTrend, Trend([0., 1., 0.]))
    iTrend += 1

    # original trajectories
    origTrajectories = computeMuTrajectories(tree)
    plotTrajectories(tree, origTrajectories)

    # proposalX = [iNode, origSlope, alpha, beta, jacobian]
    proposal1 = proposeNewSlope(tree)
    proposal2 = proposeNewSlope(tree)
    proposal3 = proposeNewSlope(tree)

    # Reverse move:
    #reverseMove(proposal3[1]*(1+alpha), proposal3[1]*(1+beta), coeffB1, coeffB2)
proposalRates()
#trendsProposals()

#plt.show()
